package br.com.fiap.webservicedevops.repository;

import br.com.fiap.webservicedevops.model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface IUserRepository extends ReactiveMongoRepository<User, Long> {

    Mono<User> findFirstByName(Mono<String> name);
}
