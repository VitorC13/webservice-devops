package br.com.fiap.webservicedevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebserviceDevopsApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebserviceDevopsApplication.class, args);
    }

}
