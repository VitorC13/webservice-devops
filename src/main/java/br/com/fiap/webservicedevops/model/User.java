package br.com.fiap.webservicedevops.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.io.Serializable;

@Document
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User implements Serializable {

    @MongoId
    private Long id;
    private String name;
    private String obs;
}
