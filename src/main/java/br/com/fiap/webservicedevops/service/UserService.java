package br.com.fiap.webservicedevops.service;

import br.com.fiap.webservicedevops.model.User;
import br.com.fiap.webservicedevops.repository.IUserRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Flux<User> findAll() {
        return userRepository.findAll();
    }

    public Flux<User> createUsers(Iterable<User> user) {
        return userRepository.insert(user);
    }

    public Mono<User> save(User user){
        return userRepository.save(user);
    }

    public Mono<User> findByName(Mono<String> name){
        return userRepository.findFirstByName(name);
    }
}
