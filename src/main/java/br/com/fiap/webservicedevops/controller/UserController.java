package br.com.fiap.webservicedevops.controller;

import br.com.fiap.webservicedevops.model.User;
import br.com.fiap.webservicedevops.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping
public class UserController {

    public final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;//
    }

    @GetMapping
    public Flux<User> findAll() {
        return userService.findAll();
    }

    @PostMapping
    public Flux<User> add(@Valid @RequestBody Iterable<User> users) {
        return userService.createUsers(users);
    }
}
