package br.com.fiap.webservicedevops;

import br.com.fiap.webservicedevops.model.User;
import br.com.fiap.webservicedevops.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
public class RepoTest {

    public UserService userService;

    @Test
    public void givenValue_whenFindAllByValue_thenFindAccount() {
        userService.save(new User(1L, "Bill", "Shit")).block();
        Flux<User> userFlux = userService.findAll();

        StepVerifier
                .create(userFlux)
                .assertNext(user -> {
                    assertEquals("Bill", user.getName());
                    assertEquals("Shit", user.getObs());
                    assertNotNull(user.getId());
                })
                .expectComplete()
                .verify();
    }

    @Test
    public void givenOwner_whenFindFirstByOwner_thenFindAccount() {
        userService.save(new User(1L, "Bill", "Shit")).block();
        Mono<User> userMono = userService
                .findByName(Mono.just("Bill"));

        StepVerifier
                .create(userMono)
                .assertNext(user -> {
                    assertEquals("Bill", user.getName());
                    assertEquals("Shit", user.getObs());
                    assertNotNull(user.getId());
                })
                .expectComplete()
                .verify();
    }

    @Test
    public void givenAccount_whenSave_thenSaveAccount() {
        Mono<User> userMono = userService.save(new User(1L, "Bill", "Shit"));

        StepVerifier
                .create(userMono)
                .assertNext(user -> assertNotNull(user.getId()))
                .expectComplete()
                .verify();
    }
}
